import React, {useEffect} from "react";
import { Route, Switch} from "react-router-dom";
import Modal from "./Modal";
import SomePagesModule from "./universalModulePage";
import CartModule from "./CartModule";
import Button from "./Button";
import {useSelector, useDispatch} from 'react-redux';
import background from "../img/bg_base.jpg";
import loadCars from "../fetch";
import {
    addCarToCart,
    deleteCarFromCart,
    modalActiveOff
}
    from "../redux/actions";
import Forms from "./Forms";

const Main = () => {

    const dispatch = useDispatch();

    const cars = useSelector((state) => state.reducerLoad.cars);
    const favoritesArr = useSelector((state) => state.reducerFavStar.favoritesArr);
    const cartArr = useSelector((state) => state.reduceCart.cartArr);
    const isLoading = useSelector((state) => state.reducerLoad.isLoading);
    const error = useSelector((state) => state.reducerLoad.error);
    const modalAction = useSelector((state) => state.reducerModal.modalAction);

    const modalForm = useSelector(state => state.reducerForm.modalForm)
    // const name = useSelector(state => state.reducerForm.name)

    useEffect(() => {
        dispatch(loadCars());
    }, [dispatch]);

    return (
        <main>
            {isLoading && <div>Loading cars... Wait</div>}
            {error && <div>{error}</div>}
            {cars && (
                <Switch>
                    <Route
                        exact path="/"
                        render={() => (
                            <SomePagesModule
                                title={'Shops with New Cars Sales'}
                                className={'baseShopPage'}
                                background={background}
                                cars={cars.map((car) => {
                                    return {
                                        ...car,
                                        isFavorite: favoritesArr.includes(car.vendorCode),
                                    };
                                })}
                                cardState={{
                                    showStarFav: true,
                                    showStars: true,
                                    showAddToCartBtn: true,
                                    showDeleteFromCartBtn: false,
                                    showCartButtonsBlock: false
                                }}
                            />
                        )}
                    />
                    <Route
                        path="/favorites"
                        render={() => (
                            <SomePagesModule
                                title={'List off Favorites Cars'}
                                className={'favShopPage'}
                                background={background}
                                cars={cars
                                    .filter((car) => favoritesArr.includes(car.vendorCode))
                                    .map((car) => {
                                        return {
                                            ...car,
                                            isFavorite: favoritesArr.includes(car.vendorCode),
                                        };
                                    })}
                                cardState={{
                                    showStarFav: true,
                                    showStars: false,
                                    showAddToCartBtn: true,
                                    showDeleteFromCartBtn: false,
                                    showCartButtonsBlock: false
                                }}
                            />
                        )}
                    />
                    <Route
                        path="/cart"
                        render={() => (
                            <CartModule
                                title={'List off Cars added in Cart'}
                                className={'cart_list'}
                                background={background}
                                modalForm={modalForm}
                                cars={cars
                                    .filter((car) =>
                                        cartArr.find((item) => car.vendorCode === item.vendorCode)
                                    )
                                    .map((car) => {
                                        return {
                                            ...car,
                                            count: cartArr.find(
                                                (item) => car.vendorCode === item.vendorCode
                                            ).count,
                                        };
                                    })}
                                cardState={{
                                    showStarFav: false,
                                    showStars: false,
                                    showAddToCartBtn: false,
                                    showDeleteFromCartBtn: true,
                                    showCartButtonsBlock: true
                                }}
                            />
                        )}
                    />
                </Switch>
            )}
            {modalAction && (
                <Modal
                    key={modalAction.vendorCode}
                    header={
                        modalAction.action === "addToCart"
                            ? "Добавление товара в корзину"
                            : "Удаление товара из корзины"
                    }
                    text={
                        modalAction.action === "addToCart"
                            ? "Продолжить добавление товара в корзину ?"
                            : `Продолжить удаление товара из корзины ?`
                    }
                    closeButton={true}
                    actions={[
                        <Button
                            componentParent={"Confirm"}
                            text={"Ok"}
                            onClick={() => {
                                modalAction.action === "addToCart"
                                    ?  dispatch(addCarToCart(modalAction.vendorCode))
                                    : dispatch(deleteCarFromCart(modalAction.vendorCode))
                                dispatch(modalActiveOff())
                            }}
                        />,

                        <Button
                            componentParent={"Cancel"}
                            text={"Cancel"}
                            onClick={()=>dispatch(modalActiveOff())}
                        />,
                    ]}
                    backgroundColor={"darkgray"}
                    colorBackHeader={"darkgray"}
                />
            )}
            {modalForm && (
                <Forms/>
            )}
        </main>
    );
};

export default Main;