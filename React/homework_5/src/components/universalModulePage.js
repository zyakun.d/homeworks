import React from "react";
import Card from "./Card";
import CartButtonsBlock from "./CartButtonsBlock";
import "./universalModulePage.scss"


const SomePagesModule = (props) => {
    const { cars, title, className, background, cardState } = props;
    const { showStarFav, showStars, showAddToCartBtn, showDeleteFromCartBtn, showCartButtonsBlock} = cardState

    return (
        <div
            className={`${className}`}
            style={{ backgroundImage: `url(${background})` }}
        >
            <div className={"categoryTitle"}>
                <h1>{title}</h1>
            </div>
            <div className={"categoryWrapper"}>
                {cars && !cars.length && <div>No cars to show</div>}
                {cars &&
                cars.map((car) => {
                    return (
                        <div className={"Card"} key={car.vendorCode}>
                            <Card
                                car={car}
                                showStarFav={showStarFav}
                                showStars={showStars}
                                showAddToCartBtn={showAddToCartBtn}
                                showDeleteFromCartBtn={showDeleteFromCartBtn}
                            />
                            {showCartButtonsBlock && <div className={"countBlock"}>
                                <CartButtonsBlock
                                    count={car.count}
                                    vendorCode={car.vendorCode}
                                />
                            </div>}
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

// Shops.propTypes = {
//     cars: PropTypes.array,
// };

export default SomePagesModule;
