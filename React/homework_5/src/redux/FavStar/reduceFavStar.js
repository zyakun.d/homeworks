
const INITIAL_STATE = {
    favoritesArr: localStorage.getItem("favorites") && localStorage.getItem("favorites") !== undefined
        ? JSON.parse(localStorage.getItem("favorites"))
        : [],
}

const reducerFavStar = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'TOGGLE_FAVORITE_STAR':
            return {
                ...state,
                favoritesArr:
                    state.favoritesArr.includes(action.payload)
                        ? state.favoritesArr.filter((item) => item !== action.payload)
                        : [...state.favoritesArr, action.payload]
            }

        default:
            return state;
    }
}

export default reducerFavStar;