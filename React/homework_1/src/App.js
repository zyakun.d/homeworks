import React from "react"
import AppModalView from "./components/AppModalView";
import './App.scss'

function App() {

    return (
        <div className={'initial_buttons'}>
            <AppModalView
                textBtn={'Open first modal'}
                backgroundColor={'#e74c3c'}
                closeButton={false}
                colorBackHeader={'#d44637'}
                colorBackBtn={'#b3382c'}
                textHeader={'Do you want to delete this file?'}
                textBase={'Once you delete this file, if won`t be possible to undo this action. Are you sure you want to delete it ? '}
            />
            <AppModalView
                textBtn={'Open second modal'}
                backgroundColor={'green'}
                closeButton={true}
                colorBackHeader={'blue'}
                colorBackBtn={'black'}
                textHeader={'Продолжим изучение Реакт ?'}
                textBase={'Создавать интерактивные пользовательские интерфейсы на React — приятно и просто. Вам достаточно описать, как части интерфейса приложения выглядят в разных состояниях.'}
            />
        </div>
    )
}

export default App;

