import React from "react";
import "./Modal.scss"

class Modal extends React.Component {

    render() {
        const {header, closeButton, text, actions, func, backgroundColor, colorBackHeader} = this.props

        return (
            <div className={'modal'}>
                <div className={'modalContainer'}>
                    <div className={'modalHeaderWrapper'} style={{backgroundColor: colorBackHeader}}>
                        <div className={'modalHeader'}>{header}</div>
                        {closeButton ? '' :
                            <div className={'crossBtn'} onClick={func}>
                                <svg className="modalCross" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 24 24">
                                    <path fill="white"
                                        d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/>
                                </svg>
                            </div>}

                    </div>
                    <div className={'modalContentText'} style={{backgroundColor: backgroundColor}}><p>{text}</p></div>
                    <div className={'modalFooterButtons'} style={{backgroundColor: backgroundColor}}>
                        {actions}
                    </div>
                </div>
                <div className={'overlay'} onClick={func}/>
            </div>

        )
    }
}

export default Modal;