import React from "react"
import Button from "./Button";
import Modal from "./Modal";

class AppModalView extends React.Component {

    state = {
        isOpen: false
    }

    onClickOk = () => {
        console.log('Ok')
        this.setState({isOpen: false})
    }

    onClickCancel = () => {
        console.log('Cancel')
        this.setState({isOpen: false})
    }


    render() {
        const {textBtn, backgroundColor, closeButton, colorBackHeader, colorBackBtn, textHeader, textBase} = this.props

        return (
            <div>
                <div>
                    <Button
                        id={textBtn==='Open first modal' ? 'left' : 'right'}
                        backgroundColor={backgroundColor}
                        text={textBtn}
                        onClick={() => {
                            this.setState({isOpen: true})
                        }
                        }
                    />
                </div>

                {this.state.isOpen && <Modal
                    header={textHeader}
                    text={textBase}
                    closeButton={closeButton}
                    actions={[
                        <Button
                            id={'modal_ok'}
                            backgroundColor={colorBackBtn}
                            text={'Ok'}
                            onClick={this.onClickOk}
                        />,

                        <Button
                            id={'modal_cancel'}
                            backgroundColor={colorBackBtn}
                            text={'Cancel'}
                            onClick={this.onClickCancel}
                        />
                    ]}
                    func={this.onClickOk}
                    backgroundColor={backgroundColor}
                    colorBackHeader={colorBackHeader}
                />}
            </div>
        )
    }
}


export default AppModalView;