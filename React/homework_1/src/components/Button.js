import React from 'react'

class Button extends React.Component {

    render() {
        const {id, backgroundColor, text, onClick} = this.props
        return (
            <div>
                <div>
                    <button id={id} onClick={onClick}
                            style={{backgroundColor: backgroundColor}}>{text}</button>
                </div>
            </div>
        )
    }
}

export default Button;
