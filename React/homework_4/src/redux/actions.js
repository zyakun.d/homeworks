//cars
export const loadData = () => ({type: 'LOAD_DATA'});
export const getSuccess = (data) => ({type: 'LOAD_SUCCESS', payload: data});
export const getFailed = (err) => ({type: 'ERROR', payload: err});

//favoritesArr
export const ToggleFavoritesStar = (vendorCode) => ({type: 'TOGGLE_FAVORITE_STAR', payload: vendorCode});

//cartArr
export const addCarToCart = (vendorCode) => ({type: 'ADD_TO_CART', payload: vendorCode});
export const deleteCarFromCart = (vendorCode) => ({type: 'DELETE_FROM_CART', payload: vendorCode});
export const cartCountPlus = (vendorCode) => ({type: 'CART_COUNT_PLUS', payload: vendorCode});
export const cartCountMinus = (vendorCode) => ({type: 'CART_COUNT_MINUS', payload: vendorCode});

//modalAction
export const modalActiveOn = (modalState) => ({type: 'MODAL_ON', payload: modalState});
export const modalActiveOff = () => ({type: 'MODAL_OFF'});