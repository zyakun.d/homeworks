const INITIAL_STATE = {
    modalAction: null,
}

const reducerModal = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'MODAL_ON':
            return {
                ...state, modalAction: action.payload
            }

        case 'MODAL_OFF':
            return {
                ...state, modalAction: null
            }

        default:
            return state;
    }
}

export default reducerModal;
