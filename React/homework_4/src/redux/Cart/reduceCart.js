
const INITIAL_STATE = {
    cartArr: localStorage.getItem("cart") && localStorage.getItem("cart") !== undefined
        ? JSON.parse(localStorage.getItem("cart"))
        : [],
}

const reduceCart = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'ADD_TO_CART':
            return {
                ...state,
                cartArr:
                    state.cartArr.find((item) => item.vendorCode === action.payload)
                        ? state.cartArr.map((item) => {
                            if (item.vendorCode === action.payload) {
                                return {vendorCode: action.payload, count: item.count + 1};
                            } else {
                                return item;
                            }
                        })
                        : [...state.cartArr, {vendorCode: action.payload, count: 1}]
            }

        case 'DELETE_FROM_CART':
            return {
                ...state,
                cartArr:
                    state.cartArr.filter((item) => item.vendorCode !== action.payload)
            }


        case 'CART_COUNT_PLUS':
            return {
                ...state,
                cartArr:
                    state.cartArr.map((item) => {
                        if (item.vendorCode === action.payload) {
                            return {vendorCode: action.payload, count: item.count + 1};
                        } else {
                            return item;
                        }
                    })
            }

        case 'CART_COUNT_MINUS':
            return {
                ...state,
                cartArr:

                    state.cartArr.map((item) => {
                        if (item.vendorCode === action.payload) {
                            return {vendorCode: action.payload, count: item.count > 0 ? item.count - 1 : 0};
                        } else {
                            return item;
                        }
                    }).filter((item) => item.count !== 0)
            }

        default:
            return state;
    }
}

export default reduceCart;