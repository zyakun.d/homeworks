import {combineReducers} from "redux";
import reducerModal from "./Modal/reduceModal";
import reduceCart from "./Cart/reduceCart";
import reducerFavStar from "./FavStar/reduceFavStar";
import reducerLoad from "./LoadCars/reducerLoad";

const rootReducer = combineReducers({
    reducerModal,
    reduceCart,
    reducerFavStar,
    reducerLoad
});

export default rootReducer;