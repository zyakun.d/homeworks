    const INITIAL_STATE = {
    cars: [],
    isLoading: null,
    error: null,
}

const reducerLoad = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'LOAD_DATA':
            return {
                ...state, isLoading: true
            }

        case 'LOAD_SUCCESS':
            return {
                ...state,
                cars: action.payload,
                isLoading: false
            }

        case 'ERROR':
            return {
                ...state,
                error: action.payload,
                isLoading: false
            }

        default:
            return state;
    }
}

export default reducerLoad;