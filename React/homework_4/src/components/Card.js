import React from "react";
import StarsRating from "stars-rating";
import Button from "./Button";
import FavoritStar from "./FavoritStar";
import "./Card.scss";
import background from "../img/bg_card.jpg";
import PropTypes from "prop-types";
import {modalActiveOn, ToggleFavoritesStar} from "../redux/actions";
import {useDispatch} from "react-redux";

function ratingChanged(newRating) {
  return newRating;
}

const Card = (props) => {
  const {
    car,
    showStarFav,
    showStars,
    showAddToCartBtn,
    showDeleteFromCartBtn,
  } = props;
  const { name, price, url, vendorCode, color, isFavorite } = car;
  const dispatch = useDispatch();

  return (
    <div
      className={"productCard"}
      style={{ backgroundImage: `url(${background})` }}
    >
      <h2>{name}</h2>
      <div>
        {showStarFav && (
          <FavoritStar
            onClickToggleFavoritesStar={() =>
                dispatch(ToggleFavoritesStar(vendorCode))
            }
            isFavorites={isFavorite}
          />
        )}
      </div>
      <img className={"productImg"} src={url} alt={`#`} />
      <h3>Model: {name}</h3>
      <div className="stars">
        {showStars && (
          <StarsRating
            count={5}
            onChange={ratingChanged}
            size={34}
            color2={"#ffd700"}
          />
        )}
      </div>
      <div>Code: {vendorCode}</div>
      <div className={"productColor"}>
        Color: <span style={{ color: `${color}` }}>{color}</span>
      </div>
      <div className={"productInfo"}>
        <div>
          <span className={"productCurrency"}>$</span>
          <span className={"productPrice"}>{price}</span>
        </div>

        {showAddToCartBtn && (
          <Button
            componentParent={"AddCart"}
            text={"ADD TO CART"}
            onClick={() => dispatch(modalActiveOn({
              action: "addToCart",
              vendorCode: vendorCode,
            }))
            }
          />
        )}

        {showDeleteFromCartBtn && (
          <div className={"cartDeleteFromCart"}>
            <i
              className="fas fa-trash-alt"
              onClick={() => dispatch(modalActiveOn({
                action: "deleteFromCart",
                vendorCode: vendorCode,
              }))}
            />
          </div>
        )}
      </div>
    </div>
  );
};

Card.propTypes = {
  car: PropTypes.object,
  showStarFav: PropTypes.bool,
  showStars: PropTypes.bool,
  showAddToCartBtn: PropTypes.bool,
  showDeleteFromCartBtn: PropTypes.bool,
};

export default Card;
