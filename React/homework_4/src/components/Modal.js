import React from "react";
import "./Modal.scss";
import PropTypes from "prop-types";
import {modalActiveOff} from "../redux/actions";
import {useDispatch} from "react-redux";

const Modal = (props) => {
  const { header, closeButton, text, actions } = props;
  const dispatch = useDispatch();

  return (
    <div className={"modal"} key={text}>
      <div className={"modalContainer"}>
        <div className={"modalHeaderWrapper"}>
          <div className={"modalHeader"}>{header}</div>
          {!closeButton ? (
            ""
          ) : (
            <div className={"crossBtn"} onClick={()=>dispatch(modalActiveOff())}>
              <svg
                className="modalCross"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
              >
                <path
                  fill="white"
                  d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"
                />
              </svg>
            </div>
          )}
        </div>
        <div className={"modalContentText"}>
          <p>{text}</p>
        </div>
        <div className={"modalFooterButtons"}>{actions}</div>
      </div>
      <div className={"overlay"} onClick={()=>dispatch(modalActiveOff())} />
    </div>
  );
};

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.array.isRequired,
};

Modal.defaultProps = {
  backgroundColor: "darkgrey",
  colorBackHeader: "darkgrey",
};

export default Modal;
