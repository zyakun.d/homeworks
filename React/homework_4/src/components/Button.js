import React from 'react';
import "./Button.scss"
import PropTypes from 'prop-types';

const Button = (props) => {


    const {componentParent, text, onClick} = props
    return (
        <div>
            <div>
                <button
                    className={`button${componentParent}`}
                    onClick={onClick}
                >{text}
                </button>
            </div>
        </div>
    )
}

Button.propTypes = {
    componentParent: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};


export default Button;
