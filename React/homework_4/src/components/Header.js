import {Link} from "react-router-dom";
import React from "react";

const Header = () => {

    return (
        <header>
            <nav>
                <ul className={'router-link'}>
                    <li><Link to='/shop'>Shops</Link></li>
                    <li><Link to='/favorites'>Favorites</Link></li>
                    <li><Link to='/cart'>Cart</Link></li>
                </ul>
            </nav>
        </header>
    )
}

export default Header