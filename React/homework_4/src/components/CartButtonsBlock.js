import React from "react";
import PropTypes from "prop-types";
import {cartCountMinus, cartCountPlus} from "../redux/actions";
import {useDispatch} from "react-redux";

const CartButtonsBlock = (props) => {
  const { vendorCode, count } = props;
    const dispatch = useDispatch();

  return (
    <div>
      <div>
        <div className={`buttonDecrement`}>
          <i
            className="fas fa-minus-square"
            onClick={() => {
                dispatch(cartCountMinus(vendorCode))
            }}
          />
        </div>
      </div>
      <div className={"countAndDeleteContainer"}>
        <div>
          <h1>Количество: {count}</h1>
        </div>
      </div>
      <div>
        <div className={`buttonIncrement`}>
          <i
            className="fas fa-plus-square"
            onClick={() => {
                dispatch(cartCountPlus(vendorCode))
            }}
          />
        </div>
      </div>
    </div>
  );
};

CartButtonsBlock.propTypes = {
  vendorCode: PropTypes.string,
  count: PropTypes.number,
  onClickCountChange: PropTypes.func,
};

export default CartButtonsBlock;
