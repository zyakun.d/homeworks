import React from "react";
import "./FavoritStar.scss";
import PropTypes from "prop-types";

const FavoritStar = (props) => {
  const { isFavorites, onClickToggleFavoritesStar } = props;

  return (
    <div className={"favoritesStar"}>
      {isFavorites ? (
        <i
          className="fas fa-star" //полная
          onClick={onClickToggleFavoritesStar}
        />
      ) : (
        <i
          className="far fa-star" //пустая
          onClick={onClickToggleFavoritesStar}
        />
      )}
    </div>
  );
};

FavoritStar.propTypes = {
  isFavorites: PropTypes.bool,
  onClickToggleFavoritesStar: PropTypes.func,
};

export default FavoritStar;
