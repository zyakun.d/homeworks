import React from "react";
import Card from "./Card";
import "./Favorites.scss";
import background from "../img/bg_base.jpg";
import PropTypes from "prop-types";

const Favorites = (props) => {
  const { cars, onClickToggleFavoritesStar, onClickAddToCart } = props;

  return (
    <div
      className={"favShopPage"}
      style={{ backgroundImage: `url(${background})` }}
    >
      <div className={"categoryTitle"}>
        <h1>New Cars Sales</h1>
      </div>
      <div className={"categoryWrapper"}>
        {cars && !cars.length && <div>No favorites cars</div>}
        {cars &&
          cars.map((car) => {
            return (
              <div className={"Card"} key={car.vendorCode}>
                <Card
                  car={car}
                  onClickToggleFavoritesStar={onClickToggleFavoritesStar}
                  onClickAddToCart={onClickAddToCart}
                  showStarFav={true}
                  showStars={false}
                  showAddToCartBtn={true}
                  showDeleteFromCartBtn={false}
                />
              </div>
            );
          })}
      </div>
    </div>
  );
};

Favorites.propTypes = {
  cars: PropTypes.array,
  onClickToggleFavoritesStar: PropTypes.func,
  onClickAddToCart: PropTypes.func,
};

export default Favorites;
