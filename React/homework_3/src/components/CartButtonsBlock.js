import React from "react";
import PropTypes from "prop-types";

const CartButtonsBlock = (props) => {
  const { onClickCountChange, vendorCode, count } = props;

  return (
    <div>
      <div>
        <div className={`buttonDecrement`}>
          <i
            className="fas fa-minus-square"
            onClick={() => {
              onClickCountChange(vendorCode, false);
            }}
          />
        </div>
      </div>
      <div className={"countAndDeleteContainer"}>
        <div>
          <h1>Количество: {count}</h1>
        </div>
      </div>
      <div>
        <div className={`buttonIncrement`}>
          <i
            className="fas fa-plus-square"
            onClick={() => {
              onClickCountChange(vendorCode, true);
            }}
          />
        </div>
      </div>
    </div>
  );
};

CartButtonsBlock.propTypes = {
  vendorCode: PropTypes.string,
  count: PropTypes.number,
  onClickCountChange: PropTypes.func,
};

export default CartButtonsBlock;
