import React from "react";
import Card from "./Card";
import PropTypes from "prop-types";
import "./Shops.scss";
import background from "../img/bg_base.jpg";

const Shops = (props) => {
  const { cars, onClickToggleFavoritesStar, onClickAddToCart } = props;

  return (
    <div
      className={"baseShopPage"}
      style={{ backgroundImage: `url(${background})` }}
    >
      <div className={"categoryTitle"}>
        <h1>New Cars Sales</h1>
      </div>
      <div className={"categoryWrapper"}>
        {cars && !cars.length && <div>No cars</div>}
        {cars &&
          cars.map((car) => {
            return (
              <div className={"Card"} key={car.vendorCode}>
                <Card
                  car={car}
                  onClickToggleFavoritesStar={onClickToggleFavoritesStar}
                  onClickAddToCart={onClickAddToCart}
                  showStarFav={true}
                  showStars={true}
                  showAddToCartBtn={true}
                  showDeleteFromCartBtn={false}
                />
              </div>
            );
          })}
      </div>
    </div>
  );
};

Shops.propTypes = {
  cars: PropTypes.array,
  onClickToggleFavoritesStar: PropTypes.func,
  onClickAddToCart: PropTypes.func,
};

export default Shops;
