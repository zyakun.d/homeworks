import React, { useEffect, useState } from "react";
import { Route, Switch } from "react-router-dom";
import Shops from "./Shops";
import Favorites from "./Favorites";
import Cart from "./Cart";
import Modal from "./Modal";
import Button from "./Button";

const LINK = "./cars.json";

const Main = () => {
  const [cars, setCars] = useState(null);
  const [favoritesArr, setFavoritesArr] = useState(
    localStorage.getItem("favorites")
      ? JSON.parse(localStorage.getItem("favorites"))
      : []
  );
  const [cartArr, setCartArr] = useState(
    localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
  );
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [modalAction, setModalAction] = useState(null);

  function closeModal() {
    setModalAction(null);
  }

  function onClickToggleFavoritesStar(vendorCode) {
    let fav;

    if (favoritesArr.includes(vendorCode)) {
      fav = favoritesArr.filter((item) => item !== vendorCode);
      console.log("убрали звезду");
    } else {
      fav = [...favoritesArr, vendorCode];
      console.log("установили звезду");
    }

    setFavoritesArr(fav);
    localStorage.setItem("favorites", JSON.stringify(fav));
  }

  function onClickAddToCart(vendorCode) {
    const foundCarItems = cartArr.find(
      (item) => item.vendorCode === vendorCode
    );

    let array;
    if (foundCarItems) {
      array = cartArr.map((item) => {
        if (item.vendorCode === vendorCode) {
          return { vendorCode: vendorCode, count: item.count + 1 };
        } else {
          return item;
        }
      });
    } else {
      array = [...cartArr, { vendorCode: vendorCode, count: 1 }];
    }

    setCartArr(array);
    localStorage.setItem("cart", JSON.stringify(array));
  }

  function onClickDeleteFromCart(vendorCode) {
    let array = cartArr.filter((item) => item.vendorCode !== vendorCode);
    setCartArr([...array]);
    localStorage.setItem("cart", JSON.stringify([...array]));
  }

  function onClickCountChange(vendorCode, action) {
    let array;

    if (action) {
      array = cartArr.map((item) => {
        if (item.vendorCode === vendorCode) {
          return { vendorCode: vendorCode, count: item.count + 1 };
        } else {
          return item;
        }
      });
    } else {
      array = cartArr.map((item) => {
        if (item.vendorCode === vendorCode) {
          return { vendorCode: vendorCode, count: item.count - 1 };
        } else {
          return item;
        }
      });
    }

    array = array.filter((item) => item.count >= 1);
    setCartArr(array);
    localStorage.setItem("cart", JSON.stringify(array));
  }

  useEffect(() => {
    setIsLoading(true);
    fetch(`${LINK}`)
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
        throw new Error("Failed to loading");
      })
      .then((resp) => {
        setCars(resp);
        setIsLoading(false);
      })
      .catch((e) => {
        setError(e);
        setIsLoading(false);
      });
  }, []);

  return (
    <main>
      {isLoading && <div>Loading cars... Wait</div>}
      {error && <div>{error}</div>}
      {cars && (
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <Shops
                cars={cars.map((car) => {
                  return {
                    ...car,
                    isFavorite: favoritesArr.includes(car.vendorCode),
                  };
                })}
                onClickToggleFavoritesStar={onClickToggleFavoritesStar}
                onClickAddToCart={(vendorCode) => {
                  setModalAction({
                    action: "addToCart",
                    vendorCode: vendorCode,
                  });
                }}
              />
            )}
          />
          <Route
            path="/favorites"
            render={() => (
              <Favorites
                cars={cars
                  .filter((car) => favoritesArr.includes(car.vendorCode))
                  .map((car) => {
                    return {
                      ...car,
                      isFavorite: favoritesArr.includes(car.vendorCode),
                    };
                  })}
                onClickToggleFavoritesStar={onClickToggleFavoritesStar}
                onClickAddToCart={(vendorCode) => {
                  setModalAction({
                    action: "addToCart",
                    vendorCode: vendorCode,
                  });
                }}
              />
            )}
          />
          <Route
            path="/cart"
            render={() => (
              <Cart
                cars={cars
                  .filter((car) =>
                    cartArr.find((item) => car.vendorCode === item.vendorCode)
                  )
                  .map((car) => {
                    return {
                      ...car,
                      count: cartArr.find(
                        (item) => car.vendorCode === item.vendorCode
                      ).count,
                    };
                  })}
                onClickCountChange={onClickCountChange}
                onClickDeleteFromCart={(vendorCode) => {
                  setModalAction({
                    action: "deleteFromCart",
                    vendorCode: vendorCode,
                  });
                }}
              />
            )}
          />
        </Switch>
      )}
      {modalAction && (
        <Modal
          header={
            modalAction.action === "addToCart"
              ? "Добавление товара в корзину"
              : "Удаление товара из корзины"
          }
          text={
            modalAction.action === "addToCart"
              ? "Продолжить добавление товара в корзину ?"
              : `Продолжить удаление товара из корзины ?`
          }
          closeButton={true}
          actions={[
            <Button
              componentParent={"Confirm"}
              text={"Ok"}
              onClick={() => {
                modalAction.action === "addToCart"
                  ? onClickAddToCart(modalAction.vendorCode)
                  : onClickDeleteFromCart(modalAction.vendorCode);
                closeModal();
              }}
            />,

            <Button
              componentParent={"Cancel"}
              text={"Cancel"}
              onClick={closeModal}
            />,
          ]}
          func={closeModal}
          backgroundColor={"darkgray"}
          colorBackHeader={"darkgray"}
        />
      )}
    </main>
  );
};

export default Main;
