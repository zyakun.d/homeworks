import React from "react";
import Card from "./Card";
import "./Cart.scss";
import CartButtonsBlock from "./CartButtonsBlock";
import PropTypes from "prop-types";

const Cart = (props) => {
  const { cars, onClickDeleteFromCart, onClickCountChange } = props;

  return (
    <div>
      <h1>Корзина</h1>
      <div className={"cart_list"}>
        {cars && !cars.length && <div>No cars in cart</div>}
        {cars &&
          cars.map((car) => {
            return (
              <div className={"Card"} key={car.vendorCode}>
                <Card
                  car={car}
                  onClickDeleteFromCart={onClickDeleteFromCart}
                  showStarFav={false}
                  showStars={false}
                  showAddToCartBtn={false}
                  showDeleteFromCartBtn={true}
                />
                <div className={"countBlock"}>
                  <CartButtonsBlock
                    onClickCountChange={onClickCountChange}
                    count={car.count}
                    vendorCode={car.vendorCode}
                  />
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
};

Cart.propTypes = {
  cars: PropTypes.array,
  onClickDeleteFromCart: PropTypes.func,
  onClickCountChange: PropTypes.func,
};

export default Cart;
