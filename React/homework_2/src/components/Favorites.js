import React from "react";
import "./Favorites.scss";
import PropTypes from 'prop-types';

const Favorites = (props) => {

    const {vendorCode, favoritesArr, onClickSetFavorites} = props;
    const fav = favoritesArr.includes(vendorCode);

    return (
        <div className={'favoritesStar'}>
            { fav ? (
                    <i className="fas fa-star" //полная
                       onClick={() => {
                           onClickSetFavorites(favoritesArr.filter((n) => n !== vendorCode))
                       }}
                    />)
                :
                (<i className="far fa-star" //пустая
                    onClick={() => {
                        onClickSetFavorites([...favoritesArr, vendorCode])
                    }}
                    />
                )}
        </div>
    )
}

Favorites.propTypes = {
    vendorCode: PropTypes.string.isRequired,
    favoritesArr: PropTypes.array,
    onClickSetFavorites: PropTypes.func.isRequired
};

Favorites.defaultProps = {
    favoritesArr: []
};

export default Favorites;
