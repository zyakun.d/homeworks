import React from 'react';
import "./Button.scss"
import PropTypes from 'prop-types';


class Button extends React.Component {

    render() {
        const {className, backgroundColor, text, onClick} = this.props
        return (
            <div>
                <div>
                    <button className={className} onClick={onClick}
                            style={{backgroundColor: backgroundColor}}>{text}</button>
                </div>
            </div>
        )
    }
}

Button.propTypes = {
    className: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

Button.defaultProps = {
    backgroundColor: 'gray'
};



export default Button;
