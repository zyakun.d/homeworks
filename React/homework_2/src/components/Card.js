import React from "react";
import StarsRating from 'stars-rating';
import Button from "./Button";
import Favorites from "./Favorites";
import "./Card.scss";
import background from "../img/bg_card.jpg";
import Modal from "./Modal";
import PropTypes from 'prop-types';

function ratingChanged(newRating) {
    return newRating;
}

class Card extends React.Component {

    state = {
        isAddToCart: false
    }

    addToCart = () => {
        this.setState({isAddToCart: true})
    }

    closeModal = () => {
        this.setState({isAddToCart: false})
    }

    render() {
        const {car, onClickAddToCart, favoritesArr, onClickSetFavorites} = this.props;
        const {name, price, url, vendorCode, color} = car;

        return (
            <div
                className={"productCard"} style={{backgroundImage: `url(${background})`}}>
                <h2>{name}</h2>
                <div>
                    <Favorites
                        favoritesArr={favoritesArr}
                        onClickSetFavorites={onClickSetFavorites}
                        vendorCode={vendorCode}
                    />
                </div>
                <img className={"productImg"} src={url} alt={`#`}/>
                <h3>Model: {name}</h3>
                <div className="stars">
                    <StarsRating
                        count={5}
                        onChange={ratingChanged}
                        size={34}
                        color2={'#ffd700'}
                    />
                </div>
                <div>Code: {vendorCode}</div>
                <div className={"productColor"}>Color: <span style={{color: `${color}`}}>{color}</span></div>
                <div className={"productInfo"}>
                    <div>
                        <span className={"productCurrency"}>$</span>
                        <span className={"productPrice"}>{price}</span>
                    </div>

                    <Button
                        className={'purchaseBtn'}
                        backgroundColor={'darkred'}
                        text={'ADD TO CART'}
                        onClick={this.addToCart}
                    />

                    {(this.state.isAddToCart) && <Modal
                        key={vendorCode}
                        header={'Добавление товара в корзину'}
                        text={'Продолжить добавление товара в корзину ?'}
                        closeButton={true}
                        actions={[
                            <Button
                                key={`ok-${vendorCode}`}
                                className={'modal_ok'}
                                backgroundColor={'orange'}
                                text={'Ok'}
                                onClick={()=>{
                                    onClickAddToCart(vendorCode)
                                    this.closeModal()
                                }}
                            />,

                            <Button
                                key={`cancel-${vendorCode}`}
                                className={'modal_cancel'}
                                backgroundColor={'orange'}
                                text={'Cancel'}
                                onClick={this.closeModal}
                            />
                        ]}
                        func={this.closeModal}
                        backgroundColor={'darkgray'}
                        colorBackHeader={'darkgray'}
                    />}
                </div>
            </div>
        )

    }
}

Card.propTypes = {
    car: PropTypes.object.isRequired,
    onClickAddToCart: PropTypes.func.isRequired,
    favoritesArr: PropTypes.array,
    onClickSetFavorites: PropTypes.func.isRequired,
};

Card.defaultProps = {
    favoritesArr: []
};


export default Card
