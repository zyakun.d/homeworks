import React from "react";
import Card from "./Card";
import propTypes from 'prop-types';
import "./Shops.scss";

class Shops extends React.Component {

    state = {
        cars: null,
        favoritesArr: localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')) : [],
        cartArr: localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [],
        isLoading: false,
        error: null
    }

    onClickSetFavorites = (fav) => {
        this.setState({favoritesArr: fav})
        localStorage.setItem('favorites', JSON.stringify(fav))
    }

    onClickAddToCart = (vendorCode) => {
        let array = [...this.state.cartArr, vendorCode]
        this.setState({cartArr: array})
        localStorage.setItem('cart', JSON.stringify(array))
    }

    componentDidMount() {

        const link = './cars.json'
        this.setState({isLoading: true})
        fetch(`${link}`)
            .then(res => {
                if (res.ok) {
                    return res.json();
                }
                throw new Error('Failed to loading')
            })
            .then(resp => {
                this.setState({cars: resp, isLoading: false})
            })
            .catch(e => {
                this.setState({error: e.message, isLoading: false})
            })
    }

    render() {
        const {cars, isLoading, error, favoritesArr} = this.state

        return (
            <div>
                <div className={"categoryTitle"}>
                    <h1>New Cars Sales</h1>
                </div>
                <div className={"categoryWrapper"}>
                    {isLoading && <div>Loading users... Wait</div>}
                    {error && <div>{error}</div>}
                    {cars && !cars.length && <div>No cars</div>}
                    {cars && cars.map((car) => {
                        return (
                            <div className={'Card'} key={(car.vendorCode)}>
                                <Card
                                    // key={(car.vendorCode)}
                                    car={car}
                                    favoritesArr={favoritesArr}
                                    onClickSetFavorites={this.onClickSetFavorites}
                                    onClickAddToCart={this.onClickAddToCart}
                                />
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

Shops.propTypes = {
    fav: propTypes.array,
    vendorCode: propTypes.string,
    cars: propTypes.object,
    isLoading: propTypes.bool,
    error: propTypes.string,
    title: propTypes.string,
    favoritesArr: propTypes.array
};


export default Shops;

