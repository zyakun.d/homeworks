import React from "react"
import Shops from "./components/Shops";
import background from "./img/bg_base.jpg";
import './App.scss'

function App() {

    return (
        <div style={{backgroundImage: `url(${background})`}}
            className={'baseShopPage'}>
            <Shops/>
        </div>
    )
}

export default App;

