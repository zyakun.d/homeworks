//cars
export const loadData = () => ({type: 'LOAD_DATA'});
export const getSuccess = (data) => ({type: 'LOAD_SUCCESS', payload: data});
export const getFailed = (err) => ({type: 'ERROR', payload: err});

//favoritesArr
export const ToggleFavoritesStar = (vendorCode) => ({type: 'TOGGLE_FAVORITE_STAR', payload: vendorCode});

//cartArr
export const addCarToCart = (vendorCode) => ({type: 'ADD_TO_CART', payload: vendorCode});
export const deleteCarFromCart = (vendorCode) => ({type: 'DELETE_FROM_CART', payload: vendorCode});
export const cartCountPlus = (vendorCode) => ({type: 'CART_COUNT_PLUS', payload: vendorCode});
export const cartCountMinus = (vendorCode) => ({type: 'CART_COUNT_MINUS', payload: vendorCode});
export const cartErase = () => ({type: 'CART_ERASE'})

//modalAction
export const modalActiveOn = (modalState) => ({type: 'MODAL_ON', payload: modalState});
export const modalActiveOff = () => ({type: 'MODAL_OFF'});

//Form
export const modalFormOn = () => ({type: 'FORM_ON'});
export const modalFormOff = () => ({type: 'FORM_OFF'});
export const nameChange = (data) => ({type: 'NAME_CHANGE', payload: data});
export const lastNameChange = (data) => ({type: 'LASTNAME_CHANGE', payload: data});
export const ageChange = (data) => ({type: 'AGE_CHANGE', payload: data});
export const deliveryAddressChange = (data) => ({type: 'DELIVERY_ADDRESS_CHANGE', payload: data});
export const mobilePhoneChange = (data) => ({type: 'PHONE_CHANGE', payload: data});
export const emailChange = (data) => ({type: 'EMAIL_CHANGE', payload: data});
export const confirmEmailChange = (data) => ({type: 'CONFIRM_EMAIL_CHANGE', payload: data});
export const passwordChange = (data) => ({type: 'PASSWORD_CHANGE', payload: data});
export const confirmPasswordChange = (data) => ({type: 'CONFIRM_PASSWORD_CHANGE', payload: data});