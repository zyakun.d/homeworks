const INITIAL_STATE = {
    modalForm: false,
    name: '',
    lastName: '',
    age: '',
    deliveryAddress: '',
    mobilePhone: '',
    email: '',
    confirmEmail: '',
    password: '',
    confirmPassword: '',

}

const reducerForm = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case 'FORM_ON':
            return {
                ...state, modalForm: true
            }

        case 'FORM_OFF':
            return {
                ...state, modalForm: false
            }

        case 'NAME_CHANGE':
            return {
                ...state, name: action.payload
            }

        case 'LASTNAME_CHANGE':
            return {
                ...state, lastName: action.payload
            }

        case 'AGE_CHANGE':
            return {
                ...state, age: action.payload
            }

        case 'DELIVERY_ADDRESS_CHANGE':
            return {
                ...state, deliveryAddress: action.payload
            }

        case 'PHONE_CHANGE':
            return {
                ...state, mobilePhone: action.payload
            }

        case 'EMAIL_CHANGE':
            return {
                ...state, email: action.payload
            }

        case 'CONFIRM_EMAIL_CHANGE':
            return {
                ...state, confirmEmail: action.payload
            }

        case 'PASSWORD_CHANGE':
            return {
                ...state, password: action.payload
            }

        case 'CONFIRM_PASSWORD_CHANGE':
            return {
                ...state, confirmPassword: action.payload
            }



        default:
            return state;
    }
}

export default reducerForm;
