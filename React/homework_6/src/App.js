import React from "react"
import './App.scss'
import Header from "./components/Header/Header";
import Main from "./components/Main";

const App = () => {

    return (
        <div className={'baseClass'}>
            <Header/>
            <Main/>
        </div>
    )
}

export default App;

