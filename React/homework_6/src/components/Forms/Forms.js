import React from "react";
import * as yup from "yup";
import {Formik} from "formik";
import './Forms.scss'
import {
    modalFormOff,
    nameChange,
    lastNameChange,
    ageChange,
    deliveryAddressChange,
    mobilePhoneChange,
    emailChange,
    confirmEmailChange,
    passwordChange,
    confirmPasswordChange,
    cartErase
} from "../../redux/actions";
import {useDispatch, useSelector} from "react-redux";

const Forms = () => {
    const validationsSсhema = yup.object().shape({
        name: yup.string().typeError('Должно быть строкой').required('Обязательно').matches(/^[a-zA-Z]+$/, "Must be only letters"),
        lastName: yup.string().typeError('Должно быть строкой').required('Обязательно').matches(/^[a-zA-Z]+$/, "Must be only letters"),
        age: yup.number().typeError('Должно быть цифрой').required('Обязательно'),
        deliveryAddress: yup.string().typeError('Должно быть строкой').required('Обязательно'),
        mobilePhone: yup.number().typeError('Должно быть числом').required('Обязательно'),
        email: yup.string().email('Введите верный емейл').required('Обязательно'),
        confirmEmail: yup.string().email('Введите верный емейл').oneOf([yup.ref('email')], 'Емейл не совпадают').required('Обязательно'),
        password: yup.string().typeError('Должно быть строкой').required('Обязательно'),
        confirmPassword: yup.string().oneOf([yup.ref('password')], 'Пароли не совпадают').required('Обязательно'),
    })

    const dispatch = useDispatch();
    let productInCart = useSelector((state) => state.reduceCart.cartArr);

    return (
        <Formik
            initialValues={{
                name: useSelector(state => state.reducerForm.name),
                lastName: useSelector(state => state.reducerForm.lastName),
                age: useSelector(state => state.reducerForm.age),
                deliveryAddress: useSelector(state => state.reducerForm.deliveryAddress),
                mobilePhone: useSelector(state => state.reducerForm.mobilePhone),
                email: useSelector(state => state.reducerForm.email),
                confirmEmail: useSelector(state => state.reducerForm.confirmEmail),
                password: useSelector(state => state.reducerForm.password),
                confirmPassword: useSelector(state => state.reducerForm.confirmPassword),
            }}
            validateOnBlur
            onSubmit={(values) => {
                console.log(`form_data`,values)
                console.log(`product_in_cart`, productInCart)
                dispatch(cartErase())

            }}
            validationSchema={validationsSсhema}
        >
            {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  isValid,
                  handleSubmit,
                  dirty
              }) => (
                  <div className={'form'}>
                      <h2>Оформление покупки</h2>
                      <p>
                          <label htmlFor="{'name'}">Имя</label><br/>
                          <input
                              className={'input'}
                              type="text"
                              name={'name'}
                              placeholder="Введите Ваше имя..."
                              onChange={(e) => { e.persist = () => {}
                                handleChange(e)
                                  dispatch(nameChange(e.target.value))
                              }}
                              onBlur={handleBlur}
                              value={values.name}
                          />
                      </p>
                      {touched.name && errors.name && <p className={'error'}>{errors.name}</p>}
                      <p>
                          <label htmlFor="{'lastName'}">Фамилия</label><br/>
                          <input
                              className={'input'}
                              type="text"
                              name={'lastName'}
                              placeholder="Введите Вашу фамилию..."
                              onChange={(e) => { e.persist = () => {}
                                  handleChange(e)
                                  dispatch(lastNameChange(e.target.value))
                              }}
                              onBlur={handleBlur}
                              value={values.lastName}
                          />
                      </p>
                      {touched.lastName && errors.lastName && <p className={'error'}>{errors.lastName}</p>}
                      <p>
                          <label htmlFor="{'age'}">Возраст</label><br/>
                          <input
                              className={'input'}
                              type="number"
                              name={'age'}
                              placeholder="Введите Ваш возраст..."
                              onChange={(e) => { e.persist = () => {}
                                  handleChange(e)
                                  dispatch(ageChange(e.target.value))
                              }}
                              onBlur={handleBlur}
                              value={values.age}
                          />
                      </p>
                      {touched.age && errors.age && <p className={'error'}>{errors.age}</p>}
                      <p>
                          <label htmlFor="{'deliveryAddress'}">Адрес доставки</label><br/>
                          <input
                              className={'input'}
                              type="text"
                              name={'deliveryAddress'}
                              placeholder="Введите адрес доставки..."
                              onChange={(e) => { e.persist = () => {}
                                  handleChange(e)
                                  dispatch(deliveryAddressChange(e.target.value))
                              }}
                              onBlur={handleBlur}
                              value={values.deliveryAddress}
                          />
                      </p>
                      {touched.deliveryAddress && errors.deliveryAddress && <p className={'error'}>{errors.deliveryAddress}</p>}
                      <p>
                          <label htmlFor="{'mobilePhone'}">Мобильный телефон</label><br/>
                          <input
                              className={'input'}
                              type="tel"
                              name={'mobilePhone'}
                              placeholder="Введите номер мобильного телефона..."
                              onChange={(e) => { e.persist = () => {}
                                  handleChange(e)
                                  dispatch(mobilePhoneChange(e.target.value))
                              }}
                              onBlur={handleBlur}
                              value={values.mobilePhone}
                          />
                      </p>
                      {touched.mobilePhone && errors.mobilePhone && <p className={'error'}>{errors.mobilePhone}</p>}
                      <p>
                          <label htmlFor="{'email'}">Электронная почта</label><br/>
                          <input
                              className={'input'}
                              type="email"
                              name={'email'}
                              placeholder="Введите Ваше email..."
                              onChange={(e) => { e.persist = () => {}
                                  handleChange(e)
                                  dispatch(emailChange(e.target.value))
                              }}
                              onBlur={handleBlur}
                              value={values.email}
                          />
                      </p>
                      {touched.email && errors.email && <p className={'error'}>{errors.email}</p>}
                      <p>
                          <label htmlFor="{'confirmEmail'}">Подтверждение электронной почты</label><br/>
                          <input
                              className={'input'}
                              type="email"
                              name={'confirmEmail'}
                              placeholder="Подтвердите email..."
                              onChange={(e) => { e.persist = () => {}
                                  handleChange(e)
                                  dispatch(confirmEmailChange(e.target.value))
                              }}
                              onBlur={handleBlur}
                              value={values.confirmEmail}
                          />
                      </p>
                      {touched.confirmEmail && errors.confirmEmail && <p className={'error'}>{errors.confirmEmail}</p>}
                      <p>
                          <label htmlFor="{'password'}">Пароль</label><br/>
                          <input
                              className={'input'}
                              type="password"
                              name={'password'}
                              placeholder="Введите Ваш пароль..."
                              onChange={(e) => { e.persist = () => {}
                                  handleChange(e)
                                  dispatch(passwordChange(e.target.value))
                              }}
                              onBlur={handleBlur}
                              value={values.password}
                          />
                      </p>
                      {touched.password && errors.password && <p className={'error'}>{errors.password}</p>}
                      <p>
                          <label htmlFor="{'confirmPassword'}">Подтвердите пароль</label><br/>
                          <input
                              className={'input'}
                              type="password"
                              name={'confirmPassword'}
                              placeholder="Подтвердите пароль..."
                              onChange={(e) => { e.persist = () => {}
                                  handleChange(e)
                                  dispatch(confirmPasswordChange(e.target.value))
                              }}
                              onBlur={handleBlur}
                              value={values.confirmPassword}
                          />
                      </p>
                      {touched.confirmPassword && errors.confirmPassword && <p className={'error'}>{errors.confirmPassword}</p>}
                      <button
                          disabled={!isValid && !dirty}
                          onClick={()=> {
                              handleSubmit()
                              dispatch(modalFormOff())
                          }}
                          type={"submit"}
                      >Купить</button>
                  </div>
            )
            }
        </Formik>

    )
}

const ModalForms = () => {

    const dispatch = useDispatch();

    return (
        <div className={"modalForm"} key={'form'}>
            <Forms/>
            <div className={"overlayForm"} onClick={()=>dispatch(modalFormOff())} />
        </div>
    );
};


export default ModalForms