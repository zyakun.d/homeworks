import React from "react";
import Modal from './Modal';
import {unmountComponentAtNode} from 'react-dom';
import {render, fireEvent, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {Provider, useSelector, useDispatch} from 'react-redux';
import {store} from "./../../redux/store";
import {modalActiveOff} from "../../redux/actions";
import configureStore from 'redux-mock-store';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';


let container = null;
let testProps;

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Testing Modal components', () => {

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container)

        testProps = {
            header: "",
            closeButton: "",
            text: "",
            actions: [],
        }
    })

    afterEach(() => {
        unmountComponentAtNode(container)
        container.remove();
        container = null;
    })

    it("Checking if the modal shows", () => {
        render(<Provider store={store}>
            <Modal {...testProps} closeButton = {true}/>
        </Provider>)

        const result = document.querySelector('.modal');
        expect(true).toBe(true);
    })


    test("Checking the display of the closing cross", () => {
        render(<Provider store={store}>
            <Modal {...testProps} closeButton={true} />
        </Provider>)
        // screen.debug();
        const result = document.querySelector('.modalCross');
        // console.log(result)
        expect(true).toBe(true);
    })

    test("Checking for the absence of a closing cross", () => {
        render(<Provider store={store}>
            <Modal {...testProps} closeButton={false}/>
        </Provider>)
        // screen.debug();
        const result = document.querySelector('.modalCross');
        // console.log(result)
        expect(false).toBe(false);
    })

    test("Checking the presence of text in the header of the Modal component", () => {
        render(<Provider store={store}>
            <Modal {...testProps} header={'testText'} closeButton={false}/>
        </Provider>)
        // screen.debug();
        const result = document.querySelector('.modalHeader');
        expect(result.textContent).toBe('testText');
    })

    test("Check display of text from props", () => {
        render(<Provider store={store}>
            <Modal {...testProps} closeButton={true} text={'Test text in props'}/>
        </Provider>)
        const result = document.querySelector('.modalContentText');
        expect(result.textContent).toBe('Test text in props');
    })

    test("Check click cross icons sends actions MODAL_OFF to redux", () => {
        const store = mockStore({todos: {modalAction: true, modalForm: ''}})
        const {getByTestId} = render(<Provider store={store}>
            <Modal {...testProps} closeButton={true} />
        </Provider>)
        // screen.debug();
        const cross = getByTestId('cross');
        expect(store.getActions().length).toBe(0);
        userEvent.click(cross);
        expect(store.getActions().length).toBe(1);
        expect(store.getActions()).toEqual([{type: 'MODAL_OFF'}]);
    })

    test("Check click overlay sends actions MODAL_OFF to redux", () => {
        const store = mockStore({todos: {modalAction: true, modalForm: ''}})
        const {getByTestId} = render(<Provider store={store}>
            <Modal {...testProps} closeButton={true} />
        </Provider>)
        // screen.debug();
        const cross = getByTestId('overlay');
        expect(store.getActions().length).toBe(0);
        userEvent.click(cross);
        expect(store.getActions().length).toBe(1);
        expect(store.getActions()).toEqual([{type: 'MODAL_OFF'}]);
    })












    // const addTodo = () => ({type: 'MODAL_OFF'})
    // it('should dispatch MODAL_OFF action', () => {
    //
    //     // Initialize mockstore with empty state
    //     const initialState = {}
    //     const store = mockStore(initialState)
    //
    //     // Dispatch the action
    //     store.dispatch(addTodo())
    //
    //     // Test if your store dispatched the expected actions
    //     const actions = store.getActions()
    //     const expectedPayload = {type: 'MODAL_OFF'}
    //     expect(actions).toEqual([expectedPayload])
    // })








    // const mockDispatch = jest.fn();
    //
    // jest.mock('react-redux', () => ({
    //     useSelector: jest.fn(),
    //     useDispatch: () => mockDispatch
    // }));

    // test('Test onClick function modalActiveOff', () => {
    //     const mockedDispatch = jest.fn();
    //     // useSelector.mockImplementation((modalActiveOff) => selectorFn(yourMockedStoreData));
    //     // useDispatch.mockReturnValue(mockedDispatch);
    //     // mount(<Provider><Modal {...testProps} /></Provider>);
    //
    //     render(<Provider store={store}> <Modal {...testProps}/> </Provider>)
    //
    //     const expectedAction =  modalActiveOff()
    //     expect(mockDispatch).toHaveBeenCalledWith(expectedAction);
    // });





    // const mockDispatch = jest.fn();
    // jest.mock('react-redux', () => ({
    //     useSelector: jest.fn(),
    //     useDispatch: () => mockDispatch
    // }));
    //
    // test("Check use function dispatch(modalActiveOff())", () => {
    //     const mockedDispatch = jest.fn();
    //     useSelector.mockImplementation((selectorFn) => selectorFn(yourMockedStoreData));
    //     useDispatch.mockReturnValue(mockedDispatch);
    //     const expectedAction = modalActiveOff();
    //     mount(<Router><Modal {...testProps}/></Router>)
    //     expect(mockDispatch).toHaveBeenCalledWith(expectedAction)
    // })












    // test("Check display buttons block", () => {
    //
    //     render(<Provider store={store}>
    //         <Modal header={''} closeButton={true} text={''} actions={[]}/>
    //     </Provider>)
    //
    //
    //     const result = document.querySelector('.modalFooterButtons');
    //
    //     console.log(result)
        // expect(result.innerHTML).toBe(<button/>);
    // })
})