import React from "react";
import Button from "./Button";
import {unmountComponentAtNode, render} from 'react-dom';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

let container = null;

beforeEach(()=>{
    container = document.createElement('div');
    document.body.appendChild(container)
})

afterEach(()=>{
    unmountComponentAtNode(container)
    container.remove();
    container = null;
})

//beforeAll()
//afterAll()


describe('Testing Button components', () => {
    test("Smoke test of Button", () => {
        render(<Button componentParent={''} text={''} onClick={()=>{}}/>, container)
    })

    test("Buttons shows className from props", () => {
        const testClassName = 'testBtn'
        render(<Button componentParent={`${testClassName}`} text={''} onClick={()=>{}}/>, container)

        const classNameBtn = document.querySelector(`.`+`button`+`${testClassName}`)
        expect(classNameBtn.className).toBe(`button${testClassName}`)
    })

    test("Buttons shows text from props", () => {
        const testText = 'testText'
        const testClassName = 'testBtn'
        render(<Button componentParent={`${testClassName}`} text={testText} onClick={()=>{}}/>, container)

        const classNameBtn = document.querySelector(`.`+`button`+`${testClassName}`)
        expect(classNameBtn.textContent).toBe(testText)

    })
})

describe('Test function click from Button component', ()=>{
    it('Test click on btn', ()=>{
        const mockOnClick = jest.fn();
        const testClassName = 'testBtn'
        const button = shallow(<Button componentParent={`${testClassName}`} text={''} onClick={mockOnClick}/>)
        button.find(`.`+`button`+`${testClassName}`).simulate('click')
        expect(mockOnClick.mock.calls.length).toEqual(1);
    })
})