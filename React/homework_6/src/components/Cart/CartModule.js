import React from "react";
import CartButtonsBlock from "./CartButtonsBlock";
import "./CartModule.scss"
import {modalActiveOn, modalFormOn} from "../../redux/actions";
import {useDispatch} from "react-redux";

const ProductInTable = (props) => {
    const {car, iter} = props
    const {name, price, url, vendorCode, color, count} = car;
    const dispatch = useDispatch();


    return (
        <tr className={`cartRow-${iter}`}>
            <th>{iter + 1}</th>
            <th>{name}</th>
            <th>
                <img className={"cartImg"} src={url} alt={`#`}/>
            </th>
            <th>{vendorCode}</th>
            <th>{color}</th>
            <th>{price}</th>
            <th>
                <CartButtonsBlock
                    count={count}
                    vendorCode={vendorCode}
                />
            </th>
            <th style={{color: 'darkred', fontSize: '40px'}}>{count * price}</th>
            <th>
                <i
                    className="fas fa-trash-alt"
                    onClick={() => dispatch(modalActiveOn({
                        action: "deleteFromCart",
                        vendorCode: vendorCode,
                    }))}
                />
            </th>
        </tr>
    )
}

function AmountInCart(carsInCart) {
    let res = 0;
    carsInCart.forEach((car) => {
        res += car.count * (Number(car.price))
    })
    return res;
}


const CartModule = (props) => {

    const {cars, className} = props;
    const dispatch = useDispatch();

    return (<div  className={`${className}`} >
            <div className={`amount`}>
                <table>
                    <tbody>
                        <tr className={`amountDown`}>
                            <th colSpan={"2"}>
                                <button
                                    className={cars.length ? `purchaseBtn` : `purchaseBtnDisable`}
                                    disabled={!cars.length}
                                    onClick={() => {
                                        dispatch(modalFormOn())
                                    }}>Оформить заказ
                                </button>
                            </th>
                        </tr>
                        <tr className={`amountTop`}>
                            <th>Итого</th>
                            <th>{`${AmountInCart(cars)} $`}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className={"categoryWrapper"}>
                {cars && !cars.length && <div>No cars to show ...</div>}
                {cars && cars.length &&
                <table className={`itemCars`}>
                    <tbody>
                        <tr>
                            <th style={{width: `2%`}}>№</th>
                            <th style={{width: `8%`}}>Модель</th>
                            <th style={{width: `30%`}}>Фото</th>
                            <th style={{width: `8%`}}>Код</th>
                            <th style={{width: `8%`}}>Цвет</th>
                            <th style={{width: `10%`}}>Цена, $</th>
                            <th style={{width: `15%`}}>Количество, шт</th>
                            <th style={{width: `14%`}}>Сумма, $</th>
                            <th style={{width: `5%`}}>Удалить из корзины</th>
                        </tr>
                    </tbody>

                    {cars &&
                    cars.map((car, i) => {
                        return (
                            <ProductInTable
                                key={i}
                                className={'row'}
                                car={car}
                                iter={i}
                            />
                        )

                    })}
                </table>
                }
            </div>
        </div>

    )

}

export default CartModule
