import {loadData, getSuccess, getFailed} from "./redux/actions";

const LINK = "./cars.json";

export const loadCars = () => (dispatch) => {

    dispatch(loadData());

        fetch(`${LINK}`)
            .then((res) => {
                if (res.ok) {
                    return res.json();
                }
                throw new Error("Failed to loading");
            })
            .then((res) => {
                dispatch(getSuccess(res))
            })
            .catch((e) => {
                dispatch(getFailed(e.message))
            });
}

export default loadCars