"use strict";

function CreateNewUser() {
        let fName = prompt("Please input firstName");
        let lName = prompt("Please input lastName");
        let bDay = prompt("Please input birthday in format dd.mm.yyyy");
        let bdArray = bDay.split(".");
    return {
        firstName: fName,
        lastName: lName,
        birthday: `${bdArray[1]}.${bdArray[0]}.${bdArray[2]}`,
        getAge(){
            let now = new Date();
            let age = Math.floor((now.getFullYear() - new Date(this.birthday).getFullYear()));
            if (now.setFullYear(1970) < new Date(this.birthday).setFullYear(1970)){
                return age - 1;
            } else {
                return age;
            }
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.lastName.slice(0).toLowerCase()
                + (new Date(this.birthday).getFullYear());
        }
    }
}
const newUser = new CreateNewUser();
console.log(newUser);
console.log(newUser.getPassword());
console.log(newUser.getAge());