    let F0 = 0;
    let F1 = 1;

    let n = prompt("Please enter the n number of the Fibonacci sequence");
    while (n === "" || isNaN(n)){
        n = prompt("Please enter the n number of the Fibonacci sequence");
    }

    function fibonacci(F0, F1, n){
        let i, Fn;
        if (n > 0) {
            for (i = 2; i <= n; i++) {
                Fn = (+F0) + (+F1);
                F0 = F1;
                F1 = Fn;
                }
        } else{
            for (i = 2; i <= (-n); i++) {
                Fn = F0 - F1;
                F0 = F1;
                F1 = Fn;
                }
        }
        return Fn;
    }
    console.log(`Fibonacci ${n} равно ${fibonacci(F0, F1, n)}`);
