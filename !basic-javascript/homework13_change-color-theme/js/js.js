"use strict";

document.addEventListener('DOMContentLoaded', function(){
       if(localStorage.length !== 0){
            document.querySelector('.style').href = localStorage.getItem('href');
        }else {
            document.querySelector('.style').setAttribute('href', "css/style.css");
        }
    });

function changeTheme(){
    const style = document.querySelector('.style');
    if(style.getAttribute('href') === "css/style.css") {
        style.setAttribute('href', "css/style2.css");
        localStorage.setItem('href', 'css/style2.css');
    } else {
        style.setAttribute('href', "css/style.css");
        localStorage.setItem('href', 'css/style.css');
    }
}

document.getElementById('toggle-theme').addEventListener('click', () => changeTheme());