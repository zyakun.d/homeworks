"use strict";

const tabs = document.querySelector('.tabs');
const buttions = document.querySelectorAll('.tabs-title');
const articles = document.querySelectorAll('.tabs-content');

tabs.onclick = function(eve){
    let target = eve.target;
    if (target.className !== 'tabs-title') return ;
    activeTabs(target);
};

function activeTabs(li){
    const targetLi = li;
    targetLi.classList.toggle('active');
    buttions.forEach(function (item, i){
        if(item !== targetLi){
          item.classList.remove('active');
        }
        if((item.getAttribute('class')) === 'tabs-title active'){
            articles[i].classList.replace('hidden', 'non-hidden');
            }else{
            articles[i].classList.replace('non-hidden', 'hidden');
        }
    });
}

