"use strict";

const buttonWrapper = document.createElement('div');
buttonWrapper.classList.add('btn-wrapper');
document.querySelector('.images-wrapper').insertAdjacentElement("beforebegin", buttonWrapper);

const buttonContinue = document.createElement('button');
buttonContinue.classList.add('btn-continue')
buttonContinue.textContent = 'Возобновить показ';
document.querySelector('.btn-wrapper').insertAdjacentElement("afterbegin", buttonContinue);

const buttonPause = document.createElement('button');
buttonPause.classList.add('btn-pause')
buttonPause.textContent = 'Прекратить';
document.querySelector('.btn-wrapper').insertAdjacentElement("afterbegin", buttonPause);

const images = document.getElementsByClassName('image-to-show');
    let flag = false;
    let thisImage = 0;
    let show;

    function nextImage(){
        images[thisImage].classList.remove('vision');
        thisImage++;
        if(thisImage > (images.length-1)){thisImage = 0}
        images[thisImage].classList.add('vision');
    }

document.querySelector('.btn-pause').addEventListener('click', function () {
    flag = true;
    clearInterval(show);
    console.log(flag);
 });

document.querySelector('.btn-continue').addEventListener('click', function () {
       if (flag) {
           flag = false;
           showImage();
       }
});

function showImage() {
    if (!flag) {
        show = setInterval(nextImage, 3000);
    }
}

 showImage();


