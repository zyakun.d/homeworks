"use strict";

const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const array2 = ["1", "2", "3", "sea", "user", 23];

function ListContents(array, id) {
    const ulTest = document.createElement('ul');
    if (id) {
        id.insertAdjacentElement("afterbegin", ulTest);
    } else {
        document.body.insertAdjacentElement("afterbegin", ulTest);
    }
    array.map(function (elem){
        let li = document.createElement('li');
        li.textContent = `${elem}`;
        ulTest.append(li);
    });
}

ListContents(array1,document.getElementById('test'));
ListContents(array2);














