"use strict";


$(document).ready(function (){
    $("#nav__menue").on('click', 'a', function (event){
        event.preventDefault();
       let id = $(this).attr('href'),
           top = $(id).offset().top;
       $("body,html").animate({scrollTop: top}, 1500);
    })
});

$(document).ready(function() {
    $(window).scroll(function () {
        if ($(this).scrollTop() > document.documentElement.clientHeight) {
            $("a.button__up").fadeIn();
        } else {
            $("a.button__up").fadeOut();
        }
    });
    $("a.button__up").click(function () {
        $("body,html").animate({scrollTop: 0}, 1500);
    });
});

$(document).ready(function (){
    $(".slide-toggle").click(function (){
        $("#Link_4").slideToggle("3000");
    });
});

