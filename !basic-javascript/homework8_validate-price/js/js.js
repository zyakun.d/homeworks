"use strict";

const inputForm = document.getElementById('inputForm');
const error = document.getElementById('error');

inputForm.onfocus = function (){
    if (this.classList.contains('error')){
        this.classList.remove('error');
        error.innerHTML = '';
    }
    document.querySelector('.spanPrice').remove();
}

inputForm.onblur = function (){
    if(this.value <= 0){
        this.classList.add('error');
        document.getElementById('inputForm').style.color = 'black';
        error.innerHTML = 'Please enter correct price';
    }else{
        let div = document.createElement('div');
        div.classList.add('wrapper');
        div.id = 'wrapper';
        document.getElementById('form').insertAdjacentElement('afterbegin', div);

        const span = document.createElement('span');
        span.classList.add('spanPrice');
        span.textContent = ` Текущая цена: ${document.getElementById('inputForm').value} $    `;
        document.getElementById('wrapper').insertAdjacentElement('afterbegin', span);

        const resetButton = document.createElement('button');
        resetButton.classList.add('reset');
        resetButton.id = 'resetButton';
        resetButton.type = 'reset';
        resetButton.onclick = resetButtonClick;
        resetButton.textContent = `X`;
        document.querySelector('.wrapper').insertAdjacentElement('beforeend', resetButton);
        document.getElementById('inputForm').style.color = 'green';
    }
};

inputForm.addEventListener('focus', ()=> inputForm.classList.add('focused'), true);
inputForm.addEventListener('blur', ()=> inputForm.classList.remove('focused'), true);


function resetButtonClick(){
    document.getElementById('form').reset();
    // document.getElementById('wrapper').remove();
    const wrappers = document.querySelectorAll('.wrapper');

        while(wrappers.length > 0){
            document.querySelector('.wrapper').remove();
        }
    }












