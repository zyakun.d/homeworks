"use strict";

const objTest = {
    name: "Ivan",
    lastName: "Petrov",
    age:   25,
    admin: false,
    user: true,
    cars: {
        avto1: "Mazda",
        avto2: "Honda",
        disel: true,
        gasoline: false,
        tth:{
            weght: 1500,
            sedan: true,
            led: undefined
            }
    }
}

const arrayTest = [1, "56", "Vasya", true, undefined, [], "Leva"];



function AnyCopy(data) {
    console.log(typeof data);

    if (typeof data === 'object') {
        function copyObj(data) {
            const newObject = {};
            for (let value in data) {
                if (typeof data[value] === 'object') {
                    newObject[value] = copyObj(data[value]);
                    continue;
                } else {
                    newObject[value] = data[value];
                }
            }
            return newObject;
        }
    }else {
        function CopyArr(data) {
            const newArr = [];
            for (let value in data) {
                if (typeof data[value] === 'object') {
                    newArr[value] = CopyArr(data[value]);
                    continue;
                } else {
                    newArr[value] = data[value];
                }
            }
            return newArr;
        }
    }
}







//     else {
//         const newArray = [];
//         for (let value in object) {
//             newArray[value] = Copy(object[value]);
//         }
//         return newArray;
//     }
// }

const objNew = AnyCopy(objTest);

console.log("objTest", objTest);
console.log("objNew", objNew);


const arrayNew = AnyCopy(arrayTest);
// const arrayNew_22 = CopyArr(arrayTest);


console.log("arrayTest", arrayTest);
console.log("arrayNew", arrayNew);
// console.log("arrayNew_22", arrayNew_22);



