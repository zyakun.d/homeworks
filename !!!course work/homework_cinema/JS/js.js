window.addEventListener('DOMContentLoaded',() => {
    render();
})

const storedTransactions = JSON.parse(localStorage.getItem('transactions'));
const transactions = storedTransactions ? storedTransactions : [];
// const transactions = storedTransactions || [];   /тоже работает

const prices = [50,100,100,400,500];

function buyTicket(row,place,clientName){

    transactions.push({
        type: "purchase",
        sum: prices[row],
        row,
        place,
        clientName,
    })

    render();
}

function returnTicket(row,place){

    const foundTransactions = transactions.filter(transaction => {

        if(transaction.row === row && transaction.place === place){
            return true;
        }

        return false;
    });

    const lastTransaction = foundTransactions.pop();


    if(!lastTransaction){
        alert("Error ...");
        return;
    }

    if(lastTransaction.type === 'refund'){
        alert("Error ... refund");
        return;
    }

    transactions.push({
        type: "refund",
        sum: lastTransaction.sum * 0.5,
        row: lastTransaction.row,
        place: lastTransaction.place,
        clientName: lastTransaction.clientName,
        purchaseTransaction: lastTransaction
    });

    render();

}

function calcTotalSum(){

    let sumPurchase = 0;
    let sumReturn = 0;
    let returnDelta = 0;

    transactions.forEach(transaction => {
        if(transaction.type === 'purchase'){
            sumPurchase += transaction.sum;
        }
        if(transaction.type === 'refund'){
            sumReturn += transaction.sum;

            returnDelta += transaction.purchaseTransaction.sum - transaction.sum;
        }
    })

    const total = sumPurchase - sumReturn;

    return {
        total: total,
        returnDelta: returnDelta
    }
}






function getPlaces(){

    const places = [
        [false,false,false],
        [false,false,false],
        [false,false,false,false],
        [false,false],
        [false,false]
    ];

    transactions.forEach(transaction => {

        if(transaction.type === 'purchase'){
            places[transaction.row][transaction.place] = true;
        }

        if(transaction.type === 'refund'){
            places[transaction.row][transaction.place] = false;
        }

    })

    return places;
}

function getBusyPlaces (places) {
    let busy = 0;
    places.forEach(row => {
        row.forEach(place => {
            if(place){
                busy += 1;
            }

        })
    })

    return busy;
}

function printPlaces(places){

    const screen_layout = document.getElementById('screen_layout');

    screen_layout.innerHTML = '';

    places.forEach((row,rowIndex) => {
        const rowDiv = document.createElement('div');
        rowDiv.className = 'row_elem';
        row.forEach((place,placeIndex) => {
            const placeDiv = document.createElement('div');
            placeDiv.className = 'place_elem';
            placeDiv.onclick = () => {
                document.getElementById('modal-form').style.display = 'block';
                document.getElementById('place').value = placeIndex;
                document.getElementById('row').value = rowIndex;
            }

            if(place){
                placeDiv.style.backgroundColor = 'red';
            }

            rowDiv.append(placeDiv);
        });
        screen_layout.append(rowDiv)
    })

}

function render(){
    const total = calcTotalSum();
    const numberOfBusyPlaces = getBusyPlaces(getPlaces());

    printPlaces(getPlaces());

    document.getElementById("number_of_seats").innerText = numberOfBusyPlaces;
    document.getElementById("total_sum").innerText = total.total + ' UAH';
    document.getElementById("return_sum").innerText = total.returnDelta + ' UAH';


    console.log('transactions',transactions)

    console.log('render')

    const tbodyElem = document.querySelector('.log tbody');

    tbodyElem.innerHTML = '';

    transactions.forEach(transaction => {
        const elem = document.createElement("tr");

        const row = document.createElement("td");
        row.innerText = transaction.row;
        elem.append(row);

        const place = document.createElement("td");
        place.innerText = transaction.place;
        elem.append(place);

        const clientName = document.createElement("td");
        clientName.innerText = transaction.clientName;
        elem.append(clientName);

        tbodyElem.append(elem);
    })


    localStorage.setItem('transactions',JSON.stringify(transactions));
}

function success(){
    alert('Success');
}
