const listFilms = document.getElementById('root');
const ulListFilms = document.createElement(`ul`);
listFilms.insertAdjacentElement("afterbegin", ulListFilms);

const requestURL = 'https://swapi.dev/api/films/'

fetch(requestURL)
    .then(response => {
        if (!response.ok) {
            throw new Error('Error HTTP, status =' + response.status)
        }
        return response.json()
    })
    .then(function (data){
        for (let i = 0; i < data.results.length; i++){

            let Film = document.createElement('div');
                Film.className = `title`;
                Film.textContent = `Название: ${data.results[i].title}`;
                ulListFilms.append(Film);

            let Episode = document.createElement('div');
                Episode.className = `episode`;
                Episode.textContent = `Эпизод: ${data.results[i].episode_id}`;
                Film.insertAdjacentElement("beforeend", Episode);

            let About = document.createElement('div');
                About.className = `about`;
                About.textContent = `Эпизод: ${data.results[i].opening_crawl}`;
                Episode.insertAdjacentElement("beforeend", About);

            let div = document.createElement('div');
                div.id = `film-${i+1}`;
                div.className = `char`;
                About.insertAdjacentElement("beforeend", div);
        }
        return data
    }).then(function (data){
        for (let i = 0; i < data.results.length; i++){

            let film = data.results[i].characters;
            let requests = film.map(url => fetch(url));
            let listFilm = document.getElementById(`film-${i+1}`)
            let divListChar = document.createElement(`div`);
            let spiner = document.createElement(`div`);
                spiner.innerHTML = `<div class="container">
                                        <div class="circle"></div>
                                      </div>`;
            listFilm.insertAdjacentElement("afterend", spiner);

            Promise.all(requests)
                .then(responses => {
                   return responses;
                })
                .then(responses => Promise.all(responses.map(r => r.json())))
                .then(function (users){
                    users.forEach(function (user) {
                        let div = document.createElement('div')
                        div.textContent = user.name;
                        listFilm.append(div);
                        spiner.className = 'hide';
                        listFilm.insertAdjacentElement("afterend", divListChar);
                    })
                }).finally(() => console.log(`Список ${i+1} сформирован`))
        }
    }).catch(Error => console.error(Error));
