class Employee {
    constructor(name, age = 0, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary || 4000;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(value) {
        if (!value){
            console.log('Имя должно содержать хотя бы одну букву!');
        } else{
            this._name = value;
        }
    }

    set age(value) {
        if (!value || value <= 0 || !Number(value)){
            console.log('Неверно указан возраст!');
        } else{
            this._age = value;
        }
    }

    set salary(value) {
        if (!value || value <= 0 || !Number(value)){
            console.log('Неверно указана зарплата!');
        } else{
            this._salary = value;
        }
    }

    get userInfo(){
        return `His name is ${this._name}. Age is ${this._age} and his base salary - ${this._salary} $`
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang = [] ) {
        super(name, age, (salary*3));
        this._lang = lang;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        if (!value || value <= 0 || !Number(value)){
            console.log('Неверно указана зарплата!');
        } else{
            this._salary = value;
        }
    }
}

const student = new Employee('Ivan', 18);
console.log('student', student);
console.log('student Salary', student.salary);

const phpDev = new Programmer('Alex', 26, 5000,['Fortran','Basic']);
console.log('phpDev', phpDev);
console.log('phpDev Salary', phpDev.salary);

const WebDev = new Programmer('Petr', 24, 5000,['Basic','Java']);
console.log('WebDev', WebDev);
console.log('WebDev Salary', WebDev.salary);

const NetDev = new Programmer('Roman', 30, 5000,['Java','Assembler']);
console.log('NetDev', NetDev);
console.log('NetDev Salary', NetDev.salary);

