// Асинхронность. Учитывая то, что JS однопоточный, асинхронность дает нам возможность в момент выполнения
// программы не останавливаться на одном процессе, а откладывать его в специальную оюласть памяти (Евент луп),
// если его выполнение отложено по времени или будет завершено через некоторый временный промежоток.
// После того как он станет готовым к выполнению, он ппередается в коллстек и после окончания всех основных
// процессов в скрипте, будет произведено выполнение очереди коллстека.

const urlIP = `https://api.ipify.org/?format=json`
const urlAdress = `http://ip-api.com/json/`

async function getClientData(){

    let responseip = await fetch(urlIP).catch(err => {
        console.error(err)});
    let clientIp = await responseip.json();

    console.log('clientIp',clientIp.ip)

    let responseAdress = await fetch(urlAdress+`${clientIp.ip}?fields=1572889`, {
        method: 'GET',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            }
        }).catch(err => {
        console.error(err)});
    let clientAdress = await responseAdress.json();

    printData(clientAdress, clientIp.ip)
    console.log('clientAdress',clientAdress)
}

function printData(obj, ip){
    clear()

    const container = document.getElementById('root');

    let title = document.createElement('div');
    title.className = `title`
    title.textContent = `Ваш IP: ${ip}. Определяем Ваше местоположение...`
    container.append(title)

    let printData = document.createElement('div');
    printData.className = `printData`;

    for (let key in obj) {

        let div = document.createElement('div')
        div.className = `list__elem`
        div.textContent = `${key}: \u00A0 ${obj[key]}`;
        printData.append(div);
        console.log(key, obj[key])
    }
        container.append(printData)
 }

function clear() {
    document.getElementById('root').innerHTML = '';
}

document.getElementById('getIp').addEventListener("click", function () {
    getClientData();
});





