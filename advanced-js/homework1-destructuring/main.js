// // Дектруктуризация не есть обязатедльным условием, но есть парвилом хорошего кода.
// // Деструктуризация нужна для упрощения нписания кода, для более удобного разбивания сложных объектов
// // на более мелкие структуры. Для более удобной работы с объектами и массивами.
//
//
// //exercise 1
//
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// const clientsAll = [new Set([...clients1, ...clients2])];

let clientsAll = [...clients1, ...clients2].filter(function (value, index,array){
    return array.indexOf(value) === index;
});

console.log('exercise 1 /', clientsAll);


//exercise 2
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

// const charactersShortInfo2 = [];
// for (const {age, lastName, name} of characters){
//     charactersShortInfo2.push({name, lastName, age});
// }
// console.log('exercise 2 /',charactersShortInfo2);

const charactersShortInfo = characters.map(function ({age, lastName, name}){
    return  {name, lastName, age};
});
console.log('exercise 2 /',charactersShortInfo);


//exercise 3

const user = {
    name2: "John",
    years: 30
};

const { name2, years: age2, isAdmin = false} = user;
console.log('exercise 3 /', `name: ${name2}, age: ${age2}, isAdmin: ${isAdmin}`);

//exercise 4 - ok

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};

console.log('exercise 4 /', fullProfile)

//exercise 5

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const booksNew = [...books, bookToAdd];
console.log('exercise 5', books, booksNew);

//exercise 6
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const employeeNew = {...employee, age: 40, salary: '1 млн $'};
console.log('exercise 6', employee, employeeNew);


//exercise 7
const array = ['value', () => 'showValue'];

// Допишите ваш код здесь

const [value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
