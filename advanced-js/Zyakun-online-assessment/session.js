function Session (user, date, questionAnswers) {
    this.user = user;
    this.date = new Date(date);
    this.questionAnswers = questionAnswers;
    this.getCorrectAnswersNumber = function (){
          return this.questionAnswers.filter(q => q.isCorrect).length;
    }
    this.getIncorrectAnswersNumber = function (){
          return this.questionAnswers.filter(q => !q.isCorrect).length;
    }
    this.getUserInfo = function (){
            const {name, lastName} = this.user;
            return `${name} ${lastName}`;
    }
    this.getYear = function (){
        return this.date.getFullYear();
    }
}

module.exports = Session;
