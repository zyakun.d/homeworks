const sessions = [
    {
    user: {
        phone: "+38067353454",
        name: "Ivan",
        lastName: "Ivanenko",
        email: "ivan@hotmail.com"
    },
    date: "2020-01-01 19:00",
    questionAnswers: [
        {
            question: "Ques 1",
            answer: "answ",
            isCorrect: true
        },
        {
            question: "Ques 2",
            answer: "answ",
            isCorrect: false
        },
        {
            question: "Ques 3",
            answer: "answ",
            isCorrect: false
        }
    ]
},
    {
        user: {
            phone: "+38067353454",
            name: "Sergey",
            lastName: "Sergeev",
            email: "ivan@hotmail.com"
        },
        date: "2020-01-01 19:00",
        questionAnswers: [
            {
                question: "Ques 1",
                answer: "answ",
                isCorrect: false
            },
            {
                question: "Ques 2",
                answer: "answ",
                isCorrect: true
            },
            {
                question: "Ques 3",
                answer: "answ",
                isCorrect: false
            }
        ]
    },
    {
        user: {
            phone: "+38067353454",
            name: "Petr",
            lastName: "Petrov",
            email: "ivan@hotmail.com"
        },
        date: "2020-01-01 19:00",
        questionAnswers: [
            {
                question: "Ques 1",
                answer: "answ",
                isCorrect: true
            },
            {
                question: "Ques 2",
                answer: "answ",
                isCorrect: true
            },
            {
                question: "Ques 3",
                answer: "answ",
                isCorrect: true
            }
        ]
    },
    {
        user: {
            phone: "+38067353454",
            name: "Andrey",
            lastName: "Andreev",
            email: "ivan@hotmail.com"
        },
        date: "2020-01-01 19:00",
        questionAnswers: [
            {
                question: "Ques 1",
                answer: "answ",
                isCorrect: false
            },
            {
                question: "Ques 2",
                answer: "answ",
                isCorrect: false
            },
            {
                question: "Ques 3",
                answer: "answ",
                isCorrect: false
            }
        ]
    }
]

module.exports = {sessions};
