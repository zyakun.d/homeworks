const questions = require('./data/questions');
const {sessions} = require('./storaje');
const Session = require('./session')

// const sessionModels = sessions.map(sessions => {
//     const {user, date, questionAnswers} = sessions;
//     return new Session(sessions.user, sessions.date, sessions.questionAnswers);
// })

const sessionModels = sessions.map(({user, date, questionAnswers}) => {
    return new Session(user, date, questionAnswers);
});

// console.log(`sessionModels`, sessionModels);

sessionModels.forEach(sessionModel => {
    console.table(sessionModel.getUserInfo(),
        sessionModel.getCorrectAnswersNumber(),
        sessionModel.getIncorrectAnswersNumber(),
        sessionModel.getYear());
})
console.log(sessionModels);

// console.log(questions.map(({text}) => {
//     return text;
// } ))
//
// console.log(sessions.map((session) => {
//     return {
//         score: session.questionAnswers.filter(a => a.isCorrect).length + `/` + session.questionAnswers.length
//     };
// } ))