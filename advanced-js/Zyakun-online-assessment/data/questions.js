const questions = [
    {
        text: "Какая переменная записана неверно?",
        options: [
            {
                text: 'var num = "STRING";',
                isCorrect: false,
            },
            {
                text: "var number = 12,5;",
                isCorrect: true,
            },
            {
                text: "var isDone = 0;",
                isCorrect: false,
            },
        ],
    },
    {
        text: "Где можно использовать JavaScript?",
        options: [
            {
                text: "Можно во всех перечисленных",
                isCorrect: true,
            },
            {
                text: "Мобильные приложения",
                isCorrect: false,
            },
            {
                text: "Веб-приложения",
                isCorrect: false,
            },
        ],
    },
    {
        text: "Где верно указан вывод данных?",
        options: [
            {
                text: 'console.log("Hello");',
                isCorrect: true,
            },
            {
                text: 'write("Hello");',
                isCorrect: false,
            },
            {
                text: 'prompt("Hello")',
                isCorrect: false,
            },
        ],
    },
    {
        text: "В чем разница между confirm и prompt?",
        options: [
            {
                text:
                    "prompt вызывает диалоговое окно с полем для ввода, confirm - окно с подтверждением",
                isCorrect: true,
            },
            {
                text: "Они ничем не отличаются;",
                isCorrect: false,
            },
            {
                text:
                    "confirm вызывает диалоговое окно с полем для ввода, prompt - окно с подтверждением",
                isCorrect: false,
            },
        ],
    },
    {
        text: "Какие значения можно хранить в переменных?",
        options: [
            {
                text: "Строки, числа с точкой, простые числа и булевые выражения",
                isCorrect: true,
            },
            {
                text: "Строки, числа с точкой и простые числа",
                isCorrect: false,
            },
            {
                text: "Только числа и строки",
                isCorrect: false,
            },
        ],
    },
    {
        text: "Что такое условный оператор?",
        options: [
            {
                text: "Оператор сравнения значений",
                isCorrect: true,
            },
            {
                text: "Конструкция, что выполняет код несколько раз",
                isCorrect: false,
            },
            {
                text: "Конструкция для создания определенной переменной",
                isCorrect: false,
            },
        ],
    },
    {
        text: "Где верно указано имя переменной?",
        options: [
            {
                text: "var num_1;",
                isCorrect: true,
            },
            {
                text: "var num-1;",
                isCorrect: false,
            },
            {
                text: "ver num;",
                isCorrect: false,
            },
        ],
    },
    {
        text: "Какие функции выполняет JS?",
        options: [
            {
                text: "Отвечает за функции на стороне клиента",
                isCorrect: true,
            },
            {
                text: "Выполняет работу с сервером",
                isCorrect: false,
            },
            {
                text: "Создает разметку на странице сайта",
                isCorrect: false,
            },
        ],
    },
    {
        text: "Какие циклы есть в языке JavaScript?",
        options: [
            {
                text: "for, while, do while",
                isCorrect: true,
            },
            {
                text: "for, forMap, foreach, while, do while",
                isCorrect: false,
            },
            {
                text: "for, forMap, foreach, while",
                isCorrect: false,
            },
        ],
    },
    {
        text: "Где верно указан запуск всплывающего окна?",
        options: [
            {
                text: 'alert ("Hi")',
                isCorrect: true,
            },
            {
                text: 'info ("Hi")',
                isCorrect: false,
            },
            {
                text: 'new alert ("Hi")',
                isCorrect: false,
            },
        ],
    },
];


module.exports = questions;
