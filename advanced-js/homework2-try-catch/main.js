const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const list = document.getElementById('root');
const ulList = document.createElement(`ul`);
list.insertAdjacentElement("afterbegin", ulList);

function prepeareBook(obj,index) {
    const {author, name, price} = obj;

    for (let key in obj){

        if(!author){
            throw new Error(`${index}, не указан Автор`);
        }
        if(!name){
            throw new Error(`${index}, не указано Название`);
        }
        if(!price){
            throw new Error(`${index}, не указана Цена`);
        }
        if (author && name && price){
            let li = document.createElement('li');
            li.textContent = (`Книга: ${obj.name}, Автор: ${obj.author}, Цена: ${obj.price}`);
            return li;
        }
    }
}

function printList(array){
    array.map(function (item, index) {
        try {
            let li = prepeareBook(item, index);
            ulList.append(li);
        }catch (err) {
            console.error(`${err.name} Ошибка в книге ${err.message}`)
        }
    })
}

printList(books);



//variant 2

// const list = document.getElementById('root');
// const ulList = document.createElement(`ul`);
// list.insertAdjacentElement("afterbegin", ulList);
//
// function makeBookView (obj){
//     return (`Книга: ${obj.name}, Автор: ${obj.author}, Цена: ${obj.price}`);
// }
//
// function verifyObj(obj,index) {
//     const {author, name, price} = obj;
//
//     for (let key in obj){
//
//         if(!author){
//             throw new Error(`${index}, не указан Автор`);
//         }
//         if(!name){
//             throw new Error(`${index}, не указано Название`);
//         }
//         if(!price){
//             throw new Error(`${index}, не указана Цена`);
//         }
//         if (author && name && price){
//             return true;
//         }
//     }
// }
//
// books.map(function (item, index) {
//     try {
//         const veryfi = verifyObj(item, index);
//         if (veryfi) {
//             let li = document.createElement('li');
//             li.textContent = makeBookView(item);
//             ulList.append(li);
//         }
//     }catch (err) {
//         console.error(`${err.name} Ошибка в книге ${err.message}`)
//     }
// });


//variant 1


// try{
//     printBookList();
// }catch (err){
//             console.log(`Ошибка. Значение "${err}" отсутствуеет`);
// }
//



// const ulList = document.createElement(`ul`);
//
// const [...{author, name, price}] = books;
//
// function makeBookView ({author, name, price}){
//     return (`Книга: ${name}, Автор: ${author}, Цена: ${price}`);
// }
//
// function printBookList() {
//     const list = document.getElementById('root');
//     list.insertAdjacentElement("afterbegin", ulList);
//
//     for (const {author, name, price} of books) {
//
//         try {
//             if(!author){
//                 throw "author";
//             }
//             if(!name){
//                 throw "name";
//             }
//             if(!price){
//                 throw "price";
//             }
//
//             if (author && name && price) {
//                 let li = document.createElement('li');
//                 li.textContent = makeBookView({author, name, price});
//                 ulList.append(li);
//             }else {
//                 console.log(`Неполные данные: Название: ${name}, Автор: ${author}, Цена: ${price}`)
//             }
//
//         }catch(err){
//             if(err === "author"){
//                 console.log(`Ошибка. Значение "author" отсутствуеет`);
//             }
//             if(err === "name"){
//                 console.log(`Ошибка. Значение "name" отсутствуеет`);
//             }
//             if(err === "price"){
//                 console.log(`Ошибка. Значение "price" отсутствуеет`);
//             }
//         }
//     }
// }
//
// printBookList();